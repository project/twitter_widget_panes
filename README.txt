This module provides a simple way to integrate widgets available on 
https://twitter.com/about/resources/widgets into drupal sites. 

This module doesn't provide a different block for "Search" and "Profile"
widgets because both are the same, if you want tweets for one user,
use @username as search.
