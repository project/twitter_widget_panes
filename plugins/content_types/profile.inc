<?php
/**
 * @file
 * This file provides a content type pane which shows
 * profile widget from twitter.
 * 
 * @ingroup twitter
 * @author Juanjo Garcia <juanjo.gcia@gmail.com>
 *  Main file for the twitter_widgets module
 */
/**
 * Callback function to supply a list of content types.
 */
function twitter_widgets_profile_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Twitter Profile'),
    'icon' => 'icon_twiter_profile.png',
    'description' => t('Twitter profile widget'),
    'category' => t('Portales 2'),
    'defaults' => array_merge(array('user' => ''), _twitter_widgets_default_values()),
  );
}

/**
 * Output function for the 'twitter profile' content type.
 */
// The function name is <code>MODULE_NAME_CT_NAME_content_type_render
function twitter_widgets_profile_content_type_render($subtype, $conf, $panel_args, $context) {
  //dsm($conf);
  $pane=new stdClass();
  $pane->content = _theme_pane_twitter_widgets_profile($conf);
  $pane->title = t('Twitter Profile for @profile', array('@profile' => $conf['user']));

  return $pane;
}

/**
 * Returns an edit form for the custom type.
 */
// The function name is <code>MODULE_NAME_CT_NAME_content_type_edit_form
function twitter_widgets_profile_content_type_edit_form(&$form, &$form_state) {
  // The current configuration
  $conf = $form_state['conf'];

  $form['user'] = array(
  '#type' => 'textfield',
  '#title' => 'Username',
  '#description' => 'profile tweets for given user, for example <em>username</em>. 
  	Note that this requires Twitter username, not @username.',
  '#default_value' => $conf['user'],
  );
  
  $form = array_merge($form, _twitter_widgets_common_form($conf));

}

function twitter_widgets_profile_content_type_edit_form_submit(&$form, &$form_state) {
  // For each part of the form defined in the 'defaults' array set when you
  // defined the content type, copy the value from the form into the array
  // of items to be saved. We don't ever want to use
  // $form_state['conf'] = $form_state['values'] because values contains
  // buttons, form id and other items we don't want stored. CTools will handle
  // the actual form submission.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

function twitter_widgets_profile_content_type_admin_title($subtype, $conf, $context) {
  return t('Twitter profile');
}

function _theme_pane_twitter_widgets_profile($conf) {
  $scrollbar = $conf['scroll']?'true':'false';
  $loop = $conf['loop']?'true':'false';
  $live = $conf['live']?'true':'false';
  
  drupal_set_html_head('<script type="text/javascript" src="http://widgets.twimg.com/j/2/widget.js"></script>');
  $output = <<< EOD
  <script>
  new TWTR.Widget({
    version: 2,
    type: 'profile',
    interval: {$conf['interval']},
    rpp: {$conf['num_tweets']},
    title: '{$conf['title']}',
    subject: '{$conf['caption']}',
    width: '100%',
    height: '{$conf['height']}',
    theme: {
      shell: {
        background: '{$conf['shell_bg']}',
        color: '{$conf['shell_fg']}',
      },
      tweets: {			
        background: '{$conf['tweet_bg']}',
        color: '{$conf['tweet_fg']}',
        links: '{$conf['tweet_link']}',
      }
    },
    features: {
      scrollbar: $scrollbar,
      loop: $loop,
      live: $live,
    behavior: '{$conf['behavior']}'
  }
  }).render().setUser('{$conf['user']}').start();
  </script>

EOD;
  return $output;
}

