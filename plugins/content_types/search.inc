<?php
/**
 * @file
 * This file provides a content type pane which shows
 * search widget from twitter.
 * 
 * @ingroup twitter
 * @author Juanjo Garcia <juanjo.gcia@gmail.com>
 *  Main file for the twitter_widgets module
 */
/**
 * Callback function to supply a list of content types.
 */
function twitter_widgets_search_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Twitter Search'),
    'icon' => 'icon_twiter_search.png',
    'description' => t('Twitter search widget'),
    'category' => t('Portales 2'),
    'defaults' => array_merge(array('search' => ''), _twitter_widgets_default_values()),
  );
}

/**
 * Output function for the 'twitter search' content type.
 */
// The function name is <code>MODULE_NAME_CT_NAME_content_type_render
function twitter_widgets_search_content_type_render($subtype, $conf, $panel_args, $context) {
  $pane=new stdClass();
  $pane->content = _theme_pane_twitter_widgets_search($conf);
  $pane->title = t('Twitter Search for @search', array('@search' => $conf['search']));

  return $pane;
}

/**
 * Returns an edit form for the custom type.
 */
function twitter_widgets_search_content_type_edit_form(&$form, &$form_state) {
  // The current configuration
  $conf = $form_state['conf'];

  $form['search'] = array(
  '#type' => 'textfield',
  '#title' => 'Search',
  '#description' => 'Search terms for tweets, for example <em>@user</em> or <em>#tag</em>.',
  '#default_value' => $conf['search'],
  );
  
  $form = array_merge($form, _twitter_widgets_common_form($conf));

}

function twitter_widgets_search_content_type_edit_form_submit(&$form, &$form_state) {
  // For each part of the form defined in the 'defaults' array set when you
  // defined the content type, copy the value from the form into the array
  // of items to be saved. We don't ever want to use
  // $form_state['conf'] = $form_state['values'] because values contains
  // buttons, form id and other items we don't want stored. CTools will handle
  // the actual form submission.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

function twitter_widgets_search_content_type_admin_title($subtype, $conf, $context) {
  return t('Twitter Search');
}

function _theme_pane_twitter_widgets_search($conf) {
  $scrollbar = $conf['scroll']?'true':'false';
  $loop = $conf['loop']?'true':'false';
  $live = $conf['live']?'true':'false';
  drupal_set_html_head('<script type="text/javascript" src="http://widgets.twimg.com/j/2/widget.js"></script>');
  $output = <<< EOD
  <script>
  new TWTR.Widget({
    version: 2,
    type: 'search',
    search: '{$conf['search']}',
    interval: {$conf['interval']},
    rpp: {$conf['num_tweets']},
    title: '{$conf['title']}',
    subject: '{$conf['caption']}',
    width: '100%',
    height: '{$conf['height']}',
    theme: {
      shell: {
        background: '{$conf['shell_bg']}',
        color: '{$conf['shell_fg']}',
      },
      tweets: {			
        background: '{$conf['tweet_bg']}',
        color: '{$conf['tweet_fg']}',
        links: '{$conf['tweet_link']}',
      }
    },
    features: {
      scrollbar: $scrollbar,
      loop: $loop,
      live: $live,
      behavior: '{$conf['behavior']}'
    }
  }).render().start();
  </script>

EOD;
  return $output;
}

